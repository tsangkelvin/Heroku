var myApp = angular.module('myApp', []);

myApp.controller('PeopleController', function ($scope) {
  	$scope.people = [
		{'name': 'Charles Babbage', 'born': 1791},
    	{'name': 'Tim Berners-Lee', 'born': 1955},
    	{'name': 'George Boole', 'born': 1815},
		{'name': 'Vinton Cerf', 'born': 1943},
		{'name': 'John Presper Eckert', 'born': 1919}
  	];
});

